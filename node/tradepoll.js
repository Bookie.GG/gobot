#!/usr/bin/env node
/*jslint node: true */
'use strict';

var bootstrap = require('./utils/bootstrap.js');
var states = require('./utils/tradeoffer_states.js');

var sessionId = process.argv[2];
var steamLogin = process.argv[3];
var steamLoginSecure = process.argv[4];
var tradeOfferId = process.argv[6];

bootstrap(function(tradeoffers) {
    tradeoffers.getOffer({tradeofferid: tradeOfferId}, function(err, offerDetails) {
        if(err) {
            console.error(err);
            process.exit(1);
        }

        offerDetails = offerDetails.response.offer;
        var status = offerDetails.trade_offer_state;

        console.log(status);
        process.exit(0);
    });
});
