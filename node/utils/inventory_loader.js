#!/usr/bin/node
/*jslint node: true */
'use strict';

var utils = require('./utils.js');
var item = utils.item;

module.exports = function(options, tradeoffers, match_fn, total, callback) {
    var out = {ids: [], names: [], items: []};
    var fn_name = "";
    var trade_options = {appId: 730, contextId: 2};

    if(options.from == "them") {
        fn_name = "loadPartnerInventory";
        trade_options.partnerSteamId = options.partnerSteamId;
    } else {
        fn_name = "loadMyInventory";
    }

    
    tradeoffers[fn_name](trade_options, function(err, inv) {
        for(var index in inv) {
            var inv_item = inv[index];
            //if(itemNamesFromMe.indexOf(inv_item.market_hash_name) > -1) {
            //      itemsFromMe.push(item(inv_item.id)); 
            //}

            var result = match_fn(inv_item);
            if(result === true) {
                out.ids.push(inv_item.id);
                out.names.push(inv_item.market_hash_name);
                out.items.push(item(inv_item.id));
            }

            if(total == out.ids.length) {
                break;
            }
        }

        if(total != out.ids.length) {
            console.error("Not all items were found");
        } else {
            callback(out.ids, out.names, out.items);
        }
    });
};
