if(process.argv.length != 7) {
    console.error("Script musi mit : argumentu sessionId, steamLogin, steamLoginSecure, apiKey, base64_data)");
    process.exit(-1);
}

var data = new Buffer(process.argv[6], 'base64');
data = data.toString('utf-8');
data = JSON.parse(data);



module.exports = {
    type: data.type,
    sessionId: process.argv[2],
    steamLogin: process.argv[3],
    steamLoginSecure: process.argv[4],
    apiKey: process.argv[5],
    steamId: data.steamid,
    token: data.token,
    message: data.message,
    items: data.items,
};
