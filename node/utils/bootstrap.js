var SteamTradeOffers = require('steam-tradeoffers');

module.exports = function(cb) {
    offers = new SteamTradeOffers();

    offers.setup(
            process.argv[2],
            process.argv[3],
            process.argv[4],
            process.argv[5],
            function(err) {
                if(err) {
                    console.error(err);
                }

                cb(offers);
            });
};
