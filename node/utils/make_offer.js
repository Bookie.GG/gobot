/*jslint node: true */
'use strict';


module.exports = function(tradeoffers, steamId, accessToken, message, itemsFromMe, itemsFromThem, callback) {
    tradeoffers.makeOffer({
        partnerSteamId: steamId,
        accessToken: accessToken,
        message: message,
        itemsFromMe: itemsFromMe,
        itemsFromThem: itemsFromThem,
    }, function(err, tradeofferid) {
        if(err) {
            console.error(err); 
        }

        if(tradeofferid !== undefined) {
            callback(tradeofferid);
        } else {
            console.error("Item does not exist in user's inventory");
            process.exit(0);
        }
    });
};
