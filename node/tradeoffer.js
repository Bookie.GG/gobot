#!/usr/bin/env node
/*jslint node: true */
'use strict';

var bootstrap = require('./utils/bootstrap.js');
var make_offer = require('./utils/make_offer.js');
var utils = require('./utils/utils.js');
var input = require('./utils/input.js');
var inventory_loader = require('./utils/inventory_loader.js');

if(process.argv.length != 7) {
    console.error("Script musi mit 5 argumentu (sessionId, steamLogin, steamLoginSecure, apiKey, base64_data)");
} else {
    var steamId     = input.steamId;
    var accessToken = input.token;
    var message     = input.message;

    var itemsFromMe = [];
    var itemsFromThem = [];

    bootstrap(function(tradeoffers) {

        var match_fn;
        var inventory_loader_options = {};

        // accepts names, finds IDs and assigns to the variable
        var itemNamesFromMe = input.items;

        if(input.type == "request") {
            inventory_loader_options = { from: 'them', partnerSteamId: steamId };

            match_fn = function(item){ 
                return (input.items.indexOf(item.id) > -1);
            };
        } else {
            inventory_loader_options = { from: 'me' };

            match_fn = function(item){ 
                return (itemNamesFromMe.indexOf(item.market_hash_name) > -1);
            };
        }

        inventory_loader(
            inventory_loader_options,
            tradeoffers, 
            match_fn,
            input.items.length, 
            function(ids, names, fetched_items) {
                if(input.type == "request") {
                    itemsFromMe = [];
                    itemsFromThem = fetched_items;
                } else {
                    itemsFromMe = fetched_items;
                    itemsFromThem = [];
                }

                make_offer(tradeoffers, steamId, accessToken, message, itemsFromMe, itemsFromThem, function(tradeofferid) {
                    var out = {};

                    out.tradeid = tradeofferid.tradeofferid;

                    out.items = {};
                    out.items.names = names;
                    out.items.ids = ids;

                    console.log(JSON.stringify(out));
                });
            }
        );
    });
}
