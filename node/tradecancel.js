#!/usr/bin/env node
/*jslint node: true */
'use strict';

var bootstrap = require('./utils/bootstrap.js');
var states = require('./utils/tradeoffer_states.js');

var sessionId = process.argv[2];
var steamLogin = process.argv[3];
var steamLoginSecure = process.argv[4];
var tradeOfferId = process.argv[6];

bootstrap(function(tradeoffers) {
    tradeoffers.cancelOffer({tradeOfferId: tradeOfferId}, function(err) {
        if(err) {
            console.error(err);
        }

        process.exit(0);    
    });
});
