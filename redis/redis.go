package redis

import (
	"gitlab.com/Bookie.GG/gobot/colors"

	// use alias to distinguish between my redis package and redigo's
	redisLib "github.com/garyburd/redigo/redis"
)

type Conn redisLib.Conn

var String = redisLib.String
var Bool = redisLib.Bool
var Strings = redisLib.Strings

func Connect() Conn {
	colors.Status.Print("Connecting to Redis... ")
	conn, err := redisLib.Dial("tcp", ":6379")

	if err != nil {
		panic(err)
	}

	colors.Success.Println("Success")

	return conn
}
