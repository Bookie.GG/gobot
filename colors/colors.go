package colors

import "github.com/fatih/color"

var Status = color.New(color.FgBlue).Add(color.Bold)
var Info = color.New(color.FgYellow)
var Success = color.New(color.FgGreen).Add(color.Bold)
var Failure = color.New(color.FgRed).Add(color.Bold)

var MagentaBold = color.New(color.FgMagenta).Add(color.Bold)
