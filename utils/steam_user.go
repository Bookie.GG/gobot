package utils

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

type UserFinder struct {
	DisplayName string `json:"steamID"`
}

type FinderResponse struct {
	D UserFinder `json:"d"`
}

func getSteamNameFromSteamId(id string) string {
	req, err := http.NewRequest("POST", "http://steamidfinder.com/ProfileLoad.asmx/LoadProfile",
		bytes.NewBufferString("{Profile: \"76561198005107171\"}"))

	if err != nil {
		panic(err)
	}

	req.Header.Set("User-Agent", "Bookie.GG Bot")
	httpClient := &http.Client{}

	req.Header.Set("Content-Type", "application/json; charset=UTF-8")

	res, err := httpClient.Do(req)

	if err != nil {
		panic(err)
	}

	read, err := ioutil.ReadAll(res.Body)

	if err != nil {
		panic(err)
	}

	var finderResponse FinderResponse

	err = json.Unmarshal(read, &finderResponse)

	if err != nil {
		panic(err)
	}

	return string(finderResponse.D.DisplayName)
}
