package utils

import (
	"encoding/json"
	"io/ioutil"

	"gitlab.com/Bookie.GG/gobot/colors"
)

type BotInfo struct {
	Username    string `json:"username"`
	Password    string `json:"password"`
	DisplayName string `json:"displayName"`
}

type BotFile struct {
	Collection []BotInfo
}

func LoadBots() []BotInfo {
	botfileContents, err := ioutil.ReadFile("./resources/bots.json")
	if err != nil {
		panic("Bots file not found @ ./resources/bots.json")
	}

	botInfoJSON := make([]BotInfo, 0)
	json.Unmarshal(botfileContents, &botInfoJSON)

	return botInfoJSON
}

func LoadBot(name string) BotInfo {
	colors.Status.Print("Loading botfile... ")

	for _, bot := range LoadBots() {
		if bot.Username == name {
			colors.Success.Println("Success")
			colors.Info.Println("> Loaded bot " + colors.MagentaBold.SprintFunc()(name))
			return bot
		}
	}

	panic("Bot " + name + " not found!")
}
