package utils

import (
	"io/ioutil"

	"github.com/Philipp15b/go-steam"
)

func ReadSentryfile(botname string) steam.SentryHash {
	sentryfile, err := ioutil.ReadFile("./resources/sentryfiles/" + botname + ".sentryfile")

	if err != nil {
		panic("Sentryfile for bot '" + botname + "' not found!")
	}

	return steam.SentryHash(sentryfile)
}
