package utils

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"regexp"

	"gitlab.com/Bookie.GG/gobot/colors"

	"github.com/Philipp15b/go-steam"
)

// stores API Key once it has been read from http
var APIKey string

func GetAPIKey() string {
	if len(APIKey) == 0 {
		panic("APIKey is not set yet!")
	}

	return APIKey
}

func LoadAPIKey(client *steam.Client) {
	APIKey = GetApiKey(client.Web.SessionId, client.Web.SteamLogin, client.Web.SteamLoginSecure)
}

func GetApiKey(sessionId, steamLogin, steamLoginSecure string) string {

	key, err := readKey(sessionId, steamLogin, steamLoginSecure)

	// if key-fetch was successful, just return it
	if err != nil {
		// otherwise register new key
		_, err := httpRegisterAPIKey("http://steamcommunity.com/dev/registerkey", sessionId, steamLogin, steamLoginSecure)
		if err != nil {
			fmt.Println("Failed!")
		} else {
			key, err = readKey(sessionId, steamLogin, steamLoginSecure)
		}
	}

	return key
}

func readKey(sessionId, steamLogin, steamLoginSecure string) (string, error) {
	colors.Status.Print("Fetching API Key... ")

	contents, err := httpLoadAPIKey("GET", "https://steamcommunity.com/dev/apikey", sessionId, steamLogin, steamLoginSecure)

	if err != nil {
		fmt.Printf("Failed. (%v)\n", err)
	} else {
		c, err := regexp.Compile("<p>Key: ([A-Z0-9]+)</p>")

		if err != nil {
			log.Fatalln(err)
		} else {
			if !c.MatchString(contents) {
				fmt.Println("Failed.")
			} else {
				colors.Success.Println("Success")
				colors.Status.Print("Loading API Key... ")
				matches := c.FindStringSubmatch(contents)
				key := matches[1]

				colors.Success.Println("Loaded")

				return key, nil
			}
		}
	}

	return "", fmt.Errorf("Could not load API Key")
}

func httpLoadAPIKey(method, url, sessionId, steamLogin, steamLoginSecure string) (contents string, err error) {
	httpClient := &http.Client{}

	req := customRequest("GET", url, sessionId, steamLogin, steamLoginSecure, nil)

	resp, err := httpClient.Do(req)

	if err != err {
		return "", err
	} else {

		body, err := ioutil.ReadAll(resp.Body)

		if err != nil {
			return "", err
		} else {
			return string(body), nil
		}
	}

	return "", fmt.Errorf("No contents")
}

func customRequest(method, url, sessionId, steamLogin, steamLoginSecure string, bodyData io.Reader) *http.Request {
	req, err := http.NewRequest(method, url, bodyData)
	_ = err

	req.Header.Set("User-Agent", "Bookie.GG Bot")

	req.AddCookie(&http.Cookie{Name: "sessionid", Value: sessionId, Path: "*", Domain: "*"})
	req.AddCookie(&http.Cookie{Name: "steamLogin", Value: steamLogin, Path: "*", Domain: "*"})
	req.AddCookie(&http.Cookie{Name: "steamLoginSecure", Value: steamLoginSecure, Path: "*", Domain: "*"})

	return req
}

func httpRegisterAPIKey(reqUrl, sessionId, steamLogin, steamLoginSecure string) (*http.Response, error) {
	fmt.Print("Registering new API key... ")

	data := url.Values{}
	data.Add("domain", "Bookie.GG")
	data.Add("agreeToTerms", "agreed")
	data.Add("sessionid", sessionId)
	data.Add("Submit", "Register")

	req := customRequest("POST", reqUrl, sessionId, steamLogin, steamLoginSecure, bytes.NewBufferString(data.Encode()))
	httpClient := &http.Client{}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	res, err := httpClient.Do(req)

	fmt.Println("Success")

	return res, err
}
