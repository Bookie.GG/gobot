package trade_states

const (
	Invalid       = 1
	Active        = 2
	Accepted      = 3
	Countered     = 4
	Expired       = 5
	Canceled      = 6
	Declined      = 7
	InvalidItems  = 8
	EmailPending  = 9
	EmailCanceled = 10
)
