package trade

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"os/exec"

	"gitlab.com/Bookie.GG/gobot/redis"
	"gitlab.com/Bookie.GG/gobot/utils"

	"github.com/Philipp15b/go-steam"
)

type Items struct {
	Names []string
	Ids   []string
}

type TradeRequest struct {
	TradeOfferId string `json:"tradeid"`
	Items        Items
}

func LoadTrade(redisConnection redis.Conn, jobID string) *TradeRequest {
	reply, err := redis.Strings(redisConnection.Do("HMGET", "trade:status:"+jobID, "tradeofferid", "items"))

	if err != nil {
		panic(err)
	}

	var itemNames []string
	err = json.Unmarshal([]byte(reply[1]), &itemNames)

	if err != nil {
		panic(err)
	}
	return &TradeRequest{
		TradeOfferId: reply[0],
		Items: Items{
			Names: itemNames,
		},
	}
}

func RequestItem(client *steam.Client, base64_data string) *TradeRequest {
	var tradeofferOutBytes []byte

	cmd := exec.Command("tradeoffer", client.Web.SessionId, client.Web.SteamLogin, client.Web.SteamLoginSecure, utils.GetAPIKey(), base64_data)

	stdout := &bytes.Buffer{}
	stderr := &bytes.Buffer{}

	cmd.Stdout = stdout
	cmd.Stderr = stderr

	err := cmd.Run()

	if err != nil {
		panic(err)
	}

	if stderr.Len() > 0 {
		errBytes, err := ioutil.ReadAll(stderr)

		if err != nil {
			panic(err)
		}

		panic(string(errBytes))
	}

	tradeofferOutBytes, err = ioutil.ReadAll(stdout)

	myJson := &TradeRequest{}

	err = json.Unmarshal(tradeofferOutBytes, myJson)

	if err != nil {
		panic(err)
	}

	return myJson
}
