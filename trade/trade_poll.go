package trade

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os/exec"

	"gitlab.com/Bookie.GG/gobot/utils"

	"github.com/Philipp15b/go-steam"
)

func PollTrade(client *steam.Client, tradeId string) int32 {
	cmd := exec.Command("tradepoll", client.Web.SessionId, client.Web.SteamLogin, client.Web.SteamLoginSecure, utils.GetAPIKey(), tradeId)

	stdout := &bytes.Buffer{}
	stderr := &bytes.Buffer{}

	cmd.Stdout = stdout
	cmd.Stderr = stderr

	err := cmd.Run()

	if err != nil {
		stderrOut, err := ioutil.ReadAll(stderr)

		if err != nil {
			panic(err)
		}

		fmt.Println(string(stderrOut))
	}

	contents, err := ioutil.ReadAll(stdout)

	if err != nil {
		panic(err)
	}

	contentsStr := string(contents)

	var pollState int32
	_, err = fmt.Sscanf(contentsStr, "%d\n", &pollState)

	if err != nil {
		panic(err)
	}

	return pollState
}
