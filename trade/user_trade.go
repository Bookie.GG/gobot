package trade

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/Philipp15b/go-steam"

	"gitlab.com/Bookie.GG/gobot/colors"
	"gitlab.com/Bookie.GG/gobot/job"
	"gitlab.com/Bookie.GG/gobot/redis"
	"gitlab.com/Bookie.GG/gobot/redis_poller"
	"gitlab.com/Bookie.GG/gobot/trade_states"
	"gitlab.com/Bookie.GG/gobot/trade_status"
)

func TradeWithUser(client *steam.Client, redisConnection redis.Conn, displayName string, redisTradeInfo *redis_poller.OfferInfo, isNew bool) {
	// must not be made when a trade
	// is retrieved (from jobId)
	// redisTradeInfo will have a bool indicating if it was
	// newly created or retreived
	// and based on that it will either request items or
	// get info from redis status
	var tradeRequest *TradeRequest

	fmt.Printf("Creating trade (%s) #%s for partner %s with %d items... ",
		redisTradeInfo.Type, redisTradeInfo.JobID, redisTradeInfo.SteamId, len(redisTradeInfo.Items))
	if isNew {
		tradeRequest = RequestItem(client, redisTradeInfo.Raw)
	} else {
		tradeRequest = LoadTrade(redisConnection, redisTradeInfo.JobID)
	}

	colors.Success.Print("Created... ")

	job.SetJobID(redisConnection, client.SteamId().ToUint64(), redisTradeInfo.JobID)

	tradeStatus := trade_status.NewTradeStatus(
		redisConnection,
		tradeRequest.TradeOfferId,
		redisTradeInfo.JobID,
		tradeRequest.Items.Names,
		strconv.FormatUint(client.SteamId().ToUint64(), 10),
		displayName,
	)

	for !tradeStatus.HasExpired() && tradeStatus.IsPending() {
		pollState := PollTrade(client, tradeRequest.TradeOfferId) // add return value (status)

		if pollState == trade_states.Active {
			time.Sleep(500 * time.Millisecond)
		} else if pollState == trade_states.Accepted {
			tradeStatus.SetAccepted()
		} else {
			// this does not appear to be called
			// status is not chaged
			tradeStatus.SetCancelled()
		}
	}

	if !tradeStatus.IsAccepted() {
		// if not accepted, cancel
		if !tradeStatus.IsCancelled() {
			tradeStatus.SetCancelled()
		}

		CancelTrade(client, tradeRequest.TradeOfferId)
	}

	// if the trade was accepted during cancelling it will make inconsistent states
	pollState := PollTrade(client, tradeRequest.TradeOfferId) // add return value (status)

	if pollState == trade_states.Active {
		colors.Failure.Println("\nTrade failed. After cancelling it is still pending. Shutting down")
		os.Exit(1)
	} else if tradeStatus.IsAccepted() || pollState == trade_states.Accepted {
		colors.Success.Println("Accepted by user!")
	} else {
		if tradeStatus.HasExpired() {
			colors.Failure.Println("Expired!")
		} else {
			colors.Failure.Println("Canceled by user!")
		}
	}

	// once trade is completed, remove it from redis
	job.ClearJobID(redisConnection, client.SteamId().ToUint64())

	time.Sleep(5 * time.Second)
}
