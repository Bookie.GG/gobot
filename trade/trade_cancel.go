package trade

import (
	"os/exec"

	"gitlab.com/Bookie.GG/gobot/utils"

	"github.com/Philipp15b/go-steam"
)

func CancelTrade(client *steam.Client, tradeId string) {
	cmd := exec.Command("tradecancel", client.Web.SessionId, client.Web.SteamLogin, client.Web.SteamLoginSecure, utils.GetAPIKey(), tradeId)

	cmd.Run()
}
