package handlers

import (
	"gitlab.com/Bookie.GG/gobot/colors"

	"github.com/Philipp15b/go-steam"
)

func WebSessionIdEventHandler(client steam.Client) {
	colors.Status.Print("Connecting to SteamCommunity... ")
	client.Web.LogOn()
	colors.Success.Println("Success")
}
