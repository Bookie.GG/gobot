package handlers

import (
	"github.com/Philipp15b/go-steam"
	"gitlab.com/Bookie.GG/gobot/colors"
)

func ConnectedEventHandler(loginInfo *steam.LogOnDetails, client steam.Client) {
	colors.Status.Print("Logging in... ")
	client.Auth.LogOn(loginInfo)
}
