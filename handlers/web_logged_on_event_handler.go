package handlers

import (
	"gitlab.com/Bookie.GG/gobot/utils"

	"github.com/Philipp15b/go-steam"
)

func WebloggedOnEventHandlers(client steam.Client) {
	// Load API Key on WebLoggedOn
	utils.LoadAPIKey(&client)
}
