package handlers

import (
	"gitlab.com/Bookie.GG/gobot/colors"

	"github.com/Philipp15b/go-steam"
	"github.com/Philipp15b/go-steam/internal/steamlang"
)

func LoggedInEventHandler(client steam.Client) {
	colors.Success.Println("Success")
	colors.Status.Print("Setting persona state... ")
	client.Social.SetPersonaState(steamlang.EPersonaState_Online)
	colors.Success.Println("Success")
}
