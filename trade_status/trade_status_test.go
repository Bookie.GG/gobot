package trade_status_test

import (
	"testing"
	"time"

	"gitlab.com/Bookie.GG/gobot/trade_status"
)

func TestNewTradeStatusBeginIsGreaterThanExpiration(t *testing.T) {
	tradeStatus := trade_status.NewTradeStatus()

	if tradeStatus.Begin.After(tradeStatus.Expires) {
		t.Fatalf("Begin of trade should be earlier than expiration! Expiration - Begin = %d", tradeStatus.Begin.Unix()-tradeStatus.Expires.Unix())
	}

}

func TestDefaultStateIsPending(t *testing.T) {
	ts := trade_status.NewTradeStatus()

	if !ts.IsPending() {
		t.Fatal("Default state should be pending!")
	}
}

func TestAccepting(t *testing.T) {
	ts := trade_status.NewTradeStatus()

	ts.SetAccepted()

	if !ts.IsAccepted() {
		t.Fatal("After calling SetAccepted, IsAccepted should return true!")
	}
}

func TestExpirationNotExpiredYet(t *testing.T) {
	ts := trade_status.NewTradeStatus()

	if ts.HasExpired() {
		t.Fatalf("Trade has expired too soon. (in less than few milliseconds)")
	}
}

func TestExpirationExpired(t *testing.T) {
	ts := trade_status.NewTradeStatus()

	// move begin and expiration a day back to test it with .Now()
	shift := -24 * time.Hour

	ts.Begin = ts.Begin.Add(shift)
	ts.Expires = ts.Expires.Add(shift)

	if !ts.HasExpired() {
		t.Fatalf("Trade has not expired after a day")
	}
}

func TestCancel(t *testing.T) {
	ts := trade_status.NewTradeStatus()

	ts.SetCancelled()

	if !ts.IsCancelled() {
		t.Fatalf("Trade is not cancelled after cancelling")
	}
}

func TestCancelledIsNotPending(t *testing.T) {
	ts := trade_status.NewTradeStatus()

	ts.SetCancelled()

	if ts.IsPending() {
		t.Fatal("Cancelled trade must not be pending")
	}
}
