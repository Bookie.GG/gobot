package trade_status

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"gitlab.com/Bookie.GG/gobot/redis"
	"gitlab.com/Bookie.GG/gobot/trade_states"
)

type TradeStatus struct {
	Status       int16
	Begin        time.Time
	Expires      time.Time
	TradeOfferId string
	JobID        string
	redisConn    redis.Conn
}

func NewTradeStatus(
	redisConnection redis.Conn,
	tradeOfferId string,
	jobId string,
	itemNames []string,
	steamId,
	displayName string,
) *TradeStatus {
	beginTime := time.Now().UTC()
	expireTime := beginTime.Add(4 * time.Minute)

	key := getTradeStatusKey(jobId)

	itemNameJson, err := json.Marshal(itemNames)

	if err != nil {
		panic(err)
	}

	redisConnection.Do("HMSET", key,
		"tradeofferid", tradeOfferId,
		"status", trade_states.Active,
		"items", itemNameJson,
		"steamId", steamId,
		"displayName", displayName,
		"createdAt", strconv.FormatInt(beginTime.Unix(), 10),
	)

	// expire after a week
	// dont want to keep it forever but
	// we want to have a little backlog
	redisConnection.Do("EXPIRE", key, 7*24*60*60)

	return &TradeStatus{
		Status:       trade_states.Active,
		Begin:        beginTime,
		Expires:      expireTime,
		TradeOfferId: tradeOfferId,
		JobID:        jobId,
		redisConn:    redisConnection,
	}
}

func getTradeStatusKey(jobId string) string {
	return fmt.Sprintf("trade:status:%s", jobId)
}

func (ts *TradeStatus) IsPending() bool {
	return ts.Status == trade_states.Active
}

func (ts *TradeStatus) IsAccepted() bool {
	return ts.Status == trade_states.Accepted
}

func (ts *TradeStatus) SetAccepted() {
	ts.redisConn.Do("HSET", getTradeStatusKey(ts.JobID), "status", trade_states.Accepted)
	ts.Status = trade_states.Accepted
}

func (ts *TradeStatus) HasExpired() bool {
	return ts.Expires.Before(time.Now().UTC())
}

// SetCancelled marks a trade as cancelled.
// I have chosen -1 as general cancel
func (ts *TradeStatus) SetCancelled() {
	ts.redisConn.Do("HSET", getTradeStatusKey(ts.JobID), "status", -1)
	ts.redisConn.Do("DEL", "trade:offer:"+ts.JobID)
	ts.Status = -1
}

func (ts *TradeStatus) IsCancelled() bool {
	return ts.Status == -1
}
