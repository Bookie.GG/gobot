package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/Bookie.GG/gobot/colors"
	"gitlab.com/Bookie.GG/gobot/handlers"
	"gitlab.com/Bookie.GG/gobot/redis"
	"gitlab.com/Bookie.GG/gobot/redis_poller"
	"gitlab.com/Bookie.GG/gobot/trade"
	"gitlab.com/Bookie.GG/gobot/utils"

	"github.com/Philipp15b/go-steam"
)

func getNowTime() int64 {
	return time.Now().UTC().Unix()
}

func main() {
	if len(os.Args) == 1 {
		fmt.Println("No bot name provided!")
		os.Exit(1)
	}

	botName := os.Args[1]
	bot := utils.LoadBot(botName)
	redisConnection := redis.Connect()

	myLoginInfo := &steam.LogOnDetails{
		Username:       bot.Username,
		Password:       bot.Password,
		SentryFileHash: utils.ReadSentryfile(bot.Username),
	}

	client := steam.NewClient()

	colors.Status.Print("Connecting to Steam... ")
	client.ConnectEurope()
	colors.Success.Println("Success")

	for {
		select {
		case event := <-client.Events():
			switch e := event.(type) {
			case *steam.ConnectedEvent:
				handlers.ConnectedEventHandler(myLoginInfo, *client)
			case *steam.LoggedOnEvent:
				handlers.LoggedInEventHandler(*client)
			case *steam.WebSessionIdEvent:
				handlers.WebSessionIdEventHandler(*client)
			case *steam.WebLoggedOnEvent:
				handlers.WebloggedOnEventHandlers(*client)
			case steam.FatalErrorEvent:
				log.Print(e)
			case error:
				log.Print(e)
			}
		default:
			// if SteamLogin is set, client is logged into web
			if len(client.Web.SteamLogin) > 0 {
				if redisTradeInfo, isNew, ok := redis_poller.Poll(client, redisConnection); ok {
					trade.TradeWithUser(client, redisConnection, bot.DisplayName, redisTradeInfo, isNew)
				} else {
					fmt.Println("waiting")
				}
			}
		}
		time.Sleep(3 * time.Second)
	}
}
