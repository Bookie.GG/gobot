package redis_poller

import (
	"encoding/base64"
	"encoding/json"
	"fmt"

	"gitlab.com/Bookie.GG/gobot/redis"

	"github.com/Philipp15b/go-steam"
)

type OfferInfo struct {
	SteamId string   `json:"steamid"`
	Token   string   `json:"token"`
	Message string   `json:"message"`
	Items   []string `json:"items"`
	Type    string
	Raw     string
	JobID   string
	New     bool
}

func Poll(client *steam.Client, redisConnection redis.Conn) (tradeoffer *OfferInfo, isNew, ok bool) {
	return getFirstTradeOfferInQueue(redisConnection, client)
}

func getFirstTradeOfferInQueue(redisConnection redis.Conn, client *steam.Client) (tradeoffer *OfferInfo, isNew, ok bool) {

	reply, jobId, isNew, ok := getTradeOfferFromRedis(redisConnection, client)

	if !ok {
		return nil, false, ok
	}

	offerInfo := decodeTradeOffer(*reply)
	offerInfo.JobID = *jobId
	return &offerInfo, isNew, true
}

func getTradeOfferFromRedis(redisConnection redis.Conn, client *steam.Client) (base64, outJobId *string, isNew, ok bool) {
	jobId, isNew := getJobID(redisConnection, client)

	reply, err := redis.String(redisConnection.Do("GET", "trade:offer:"+jobId))

	if err != nil {
		return nil, nil, false, false
	}

	return &reply, &jobId, isNew, true
}

func getJobID(redisConnection redis.Conn, client *steam.Client) (reply string, isNew bool) {

	key := fmt.Sprintf("bot:%d:job", client.SteamId().ToUint64())

	exists, err := redis.Bool(redisConnection.Do("EXISTS", key))

	if err != nil {
		panic(err)
	}

	var jobId string

	if exists {
		isNew = false
		jobId, err = redis.String(redisConnection.Do("GET", key))

		if err != nil {
			panic(err)
		}

		offerExists, err := redis.Bool(redisConnection.Do("EXISTS", "trade:offer:"+jobId))

		if err != nil {
			panic(err)
		}

		if !offerExists {
			isNew = true
			jobId, err = redis.String(redisConnection.Do("LPOP", "trade:queue"))

			if err != nil {
				panic(err)
			}
		}

	} else {
		isNew = true
		jobId, err = redis.String(redisConnection.Do("LPOP", "trade:queue"))
	}

	return jobId, isNew
}

func decodeTradeOffer(base64Data string) OfferInfo {
	requestDecodedBytes, err := base64.StdEncoding.DecodeString(base64Data)

	if err != nil {
		panic(err)
	}

	var requestJSON OfferInfo
	err = json.Unmarshal(requestDecodedBytes, &requestJSON)

	if err != nil {
		panic(err)
	}

	requestJSON.Raw = base64Data
	return requestJSON
}
