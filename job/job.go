package job

import (
	"fmt"

	"gitlab.com/Bookie.GG/gobot/redis"
)

func SetJobID(redis redis.Conn, steamId uint64, tradeofferid string) {
	redis.Do("SET", fmt.Sprintf("bot:%d:job", steamId), tradeofferid)
}

func ClearJobID(redis redis.Conn, steamId uint64) {
	redis.Do("DEL", fmt.Sprintf("bot:%d:job", steamId))
}
